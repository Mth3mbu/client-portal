import faker from "faker";

const repository = {
  get Clients() {
    return [...Array(500).keys()].map((key) => {
      return {
        id: key,
        name: faker.name.firstName(),
        surname: faker.name.lastName(),
        address: `${faker.address.city()}, ${faker.address.country()}`,
        dateOfBirth: generateDateBirth(),
      };
    });
  },

  get Client() {
    return {
      id: faker.random.number(1000),
      name: faker.name.firstName(),
      surname: faker.name.lastName(),
      address: `${faker.address.city()}, ${faker.address.country()}`,
      dateOfBirth: generateDateBirth(),
    };
  },
};

function generateDateBirth() {
  const dateOfBirth = faker.date.between("1986-01-01", "2021-01-05");
  const days = ["Sun", "Mon", "Tues", "Wed", "Thur", "Fri", "Sat"];
  const day = days[dateOfBirth.getDay()];
  const month = new Intl.DateTimeFormat("en", { month: "short" }).format(
    dateOfBirth
  );
  return `${day}, ${month} ${dateOfBirth.getDate()}`;
}

export default repository;
