import './App.css';
import AppBar from '../src/Components/App-bar/AppBar';
import ListView from '../src/Components/List/ListView';

function App() {

  return (
    <div className="App">
     <AppBar/>
     <ListView/>
    </div>
  );
}

export default App;
