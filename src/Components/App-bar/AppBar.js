import React from "react";
import styled from "styled-components";
import ArrowBackIosIcon from "@material-ui/icons/ArrowBackIos";

const Appbar = styled.div`
  color: red;
  height: 50px;
  background-color:#00b8d4 ;
  display: flex;
  justify-content: space-around;
`;

const Title = styled.h3`
  color: White;
  padding-bottom: 5px;
  margin: auto;
  padding-right:5px;
`;

const ToolBar = () => {
  return (
    <Appbar>
      <ArrowBackIosIcon fontSize="small" style={{ color: "White", paddingTop:'15px', paddingLeft:'5px' }} />
      <Title>Clients</Title>
    </Appbar>
  );
};

export default ToolBar;
