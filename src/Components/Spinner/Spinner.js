import React from "react";
import CircularProgress from "@material-ui/core/CircularProgress";
import styled from "styled-components";

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  margin: auto;
`;

const Spinner = () => {
  return (
    <Wrapper>
      <CircularProgress style={{color:'#00b8d4'}}/>
    </Wrapper>
  );
};

export default Spinner;
