import React from "react";
import repository from "../../Repository/ClientsRepository";
import AppSpinner from "../Spinner/Spinner";
import Fab from "@material-ui/core/Fab";
import AddIcon from "@material-ui/icons/Add";
import styled from "styled-components";
import SnackBar from "../SnackBar/SnackBar";
import Tooltip from "@material-ui/core/Tooltip";
import {
  List,
  AutoSizer,
  CellMeasurer,
  CellMeasurerCache,
} from "react-virtualized";
import Row from "./Row";

const Wrapper = styled.div`
  width: 100%;
  height: 93vh;
`;

const FabWrapper = styled.div`
  position: absolute;
  overflow: auto;
  margin-bottom: 30px;
  margin-right: 15px;
  right: 0;
  z-index: 1;
  bottom: 0;
`;

let clientName = "";

const ListView = () => {
  const cache = React.useRef(
    new CellMeasurerCache({
      fixedWidth: true,
      defaultHeight: 100,
    })
  );

  const [clientsState, setClientsState] = React.useState([]);

  const simulateApiCall = (response, isSnackbarOpen) => {
    const apiCall = setTimeout(() => {
      setClientsState({ clients: response, openSnackbar: isSnackbarOpen });
      clearInterval(apiCall);
    }, 3000);
  };

  const onSnackbarCloseHandler = () => {
    const updatedState = { ...clientsState };
    updatedState.openSnackbar = false;

    setClientsState(updatedState);
  };

  const addClient = () => {
    const updatedClients = [...clientsState.clients];
    const client = repository.Client;
    clientName = `${client.name} ${client.surname}`;
    client.id = updatedClients.length;
    updatedClients.push(client);

    simulateApiCall(updatedClients, true);
  };

  React.useEffect(() => {
    simulateApiCall(repository.Clients, false);
  }, []);

  let content = <AppSpinner />;

  if (clientsState?.clients?.length > 0) {
    content = (
      <Wrapper>
        <FabWrapper>
          <Tooltip title="CLick here to auto generate and add client in the list">
            <Fab
              style={{ backgroundColor: "#00b8d4", color: "#fff" }}
              aria-label="add"
              onClick={addClient}
            >
              <AddIcon />
            </Fab>
          </Tooltip>
        </FabWrapper>
        <AutoSizer>
          {({ width, height }) => (
            <List
              width={width}
              height={height}
              rowHeight={cache.current.rowHeight}
              deferredMeasurementCache={cache.current}
              rowCount={clientsState.clients.length}
              rowRenderer={({ key, index, style, parent }) => {
                const client = clientsState.clients[index];
                let results = <AppSpinner />;

                if (client !== undefined) {
                  results = (
                    <Row
                      firstName={client.name}
                      lastName={client.surname}
                      address={client.address}
                      dateOfBirth={client.dateOfBirth}
                    />
                  );
                }

                return (
                  <CellMeasurer
                    key={key}
                    cache={cache.current}
                    parent={parent}
                    columnIndex={0}
                    rowIndex={index}
                  >
                    <div style={style}>{results}</div>
                  </CellMeasurer>
                );
              }}
            />
          )}
        </AutoSizer>

        <SnackBar
          isOpen={clientsState.openSnackbar}
          clientName={clientName}
          onClose={onSnackbarCloseHandler}
        />
      </Wrapper>
    );
  }

  return <div>{content}</div>;
};

export default ListView;
