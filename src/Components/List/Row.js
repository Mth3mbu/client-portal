import React from "react";
import ArrowForwardIosIcon from "@material-ui/icons/ArrowForwardIos";
import styled from "styled-components";
import ProfilePicture from "./Profile";
import Content from "./Content";

const Wrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  padding: 5px;
  align-self: center;
  border-bottom: 1px solid #eceff1;
`;

const ViewButton = styled.div`
  float: right;
  align-items: center;
  align-self: center;
`;

const Row = (props) => {

  return (
    <Wrapper>
      <ProfilePicture name={props.firstName} surname={props.lastName} />
      <Content
        firstName={props.firstName}
        lastName={props.lastName}
        address={props.address}
        dateOfBirth={props.dateOfBirth}
      />
      <ViewButton>
        <ArrowForwardIosIcon fontSize="small" style={{ color: "#9E9E9E" }} />
      </ViewButton>
    </Wrapper>
  );
};

export default Row;
