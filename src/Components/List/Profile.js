import React from "react";
import styled from "styled-components";

const Wrapper = styled.div`
  background-color: #e6f2ff;
  display: inline-block;
  width: 60px;
  height: 60px;
  border-radius: 50%;
  justify-content: center;
  text-align: center;
  object-fit: cover;
`;

const ProfileText = styled.div`
  margin: auto;
  padding-top: 18px;
  font-weight: bold;
`;

const ProfilePicture = (props) => {
  const profile = `${props.name.substring(0, 1)} ${props.surname.substring(
    0,
    1
  )}`;

  return (
    <Wrapper>
      <ProfileText>{profile}</ProfileText>
    </Wrapper>
  );
};
export default ProfilePicture;
