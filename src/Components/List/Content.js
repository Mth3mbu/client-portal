import React from "react";
import styled from "styled-components";

const Wrapper = styled.div`
  flex-direction: column;
  padding-left: 20px;
  flex-basis: 90%;
`;
const Paragraph = styled.p`
  margin-block-start: 1px;
  margin-block-end: 1px;
`;
const Content = (props) => {
  return (
    <Wrapper>
      <Paragraph>
        <b>
          {props.firstName} {props.lastName}.
        </b>
      </Paragraph>
      <Paragraph>{props.address}</Paragraph>
      <Paragraph>Last Booking: {props.dateOfBirth}</Paragraph>
    </Wrapper>
  );
};

export default Content;
