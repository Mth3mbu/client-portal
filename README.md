# Client Portal

# 1 How to run the App

1. Clone the project from the repo.
2. Open the project with VS Code or any JS IDE.

3. Make sure your VS Code terminal points to client-portal>.
4. Run npm install
5. Run npm start and you are good to go!!

# 2 Set up the number of records to be dispalyed

1. Navigate to Repository folder inside the project
2. Open ClientsRepository.js
3. Go to Line 5(five) on get Clients() property
4. Change Array('replace this value with an integer').keys() to an integer of your choice and
that integer will be the total number of records to be displayed.
